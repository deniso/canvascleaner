import os

indir="/files/live/here/"
outdir=""
channels=["1","2","3"]
variables=["a","b","c"]
titlex={
        "a": "variable a",
        "b": "variable b",
        "c": "variable c",
       }
label={
        "1": "channel 1",
        "2": "channel 2",
        "3": "channel 3",
      }
linecolor={
           "1": 1,
           "2": 8,
           "3": 9,
          }

for channel in channels:
    for variable in variables:

        card="card_%s_%s.dat" % (variable,channel)

        f=open(outdir + card,"w+")

        f.write("file: %soutput_%s_%s.root\n" % (indir,channel,variable))
        f.write("name: histogram_%s_%s\n" % (channel,variable))
        f.write("title: %s\n" % (label[channel]))
        f.write("titley: y-title\n")
        f.write("titlex: %s\n" % (titlex[variable] if variable in titlex else variable))
        f.write("minx: 0.\n")
        f.write("maxx: 10.\n")
        f.write("miny: 0.\n")
        f.write("maxy: 10.\n")
        f.write("logy: 0\n")
        f.write("logx: 0\n")
        f.write("linecolor: %s\n" % linecolor[channel])
        f.write("atlas: Simulation\n")
        f.write("atlasx: 0.6\n")
        f.write("latex:  %s\n" % (label[channel]))
        f.write("latexx: 0.6\n")
        f.write("latexy: 0.77\n")
                                
        f.close()
