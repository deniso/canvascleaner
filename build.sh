lsetup cmake
mkdir -p build
cd build
rm -f CMakeCache.txt
cmake ../source/canvascleaner
make
cd ..

echo
echo 'canvascleaner ready to run using ./build/canvascleaner path/to/card.dat'
