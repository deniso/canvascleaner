Set it up:
```
source setup.sh
source build.sh #first time
```
Make a card. The minimal contents of, say, `mycard.dat` is as follows:
```
file: path/to/my/ROOT/file.root
name: name_of_TH1D_or_TGraph_or_TCanvas_or_TMultiGraph_or_TTree_etc
```
Then you can run by
```
./build/canvascleaner  path/to/card.dat
```
Several config fields are supported for formatting the output plot. See `data/card.dat` for some ideas.

When you have many canvases or objects that you want to overlay, multiple cards can be passed as arguments to canvascleaner. Keeping the style streamlined for an array of perhaps kinematic plots can be done using a python script similar to the example in `scripts/makeCardsExample.py`. Have fun.
