#ifndef __HEAD_H__
#define __HEAD_H__

#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include <TStyle.h>
#include "TROOT.h"
#include "TEnv.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TGraphAsymmErrors.h"
#include "TF1.h"
#include "TTree.h"
#include "TChain.h"
#include "TObjArray.h"
#include "TClass.h"
#include "TMath.h"
#include "TLatex.h"
#include "TObject.h"
#include "TLegend.h"
#include "TLine.h"
#include "TCanvas.h"

#include "AtlasStyle.h"
#include "AtlasLabels.h"

using namespace std;

TCanvas* canvascombiner(TObjArray*,TString);
TCanvas* canvascleaner(TString,int,int);
void canvaswriter(TCanvas*,TString);

#endif
